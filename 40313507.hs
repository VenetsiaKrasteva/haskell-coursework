-- QUESTION 1: Sets

complement :: (Eq a) => [a] -> [a] -> Maybe [a]
complement xs ys 
	| subset xs ys = Just(filter (not.(`elem` xs))ys)
	|otherwise = Nothing

toMultiset :: (Eq a) => [a] -> [(a,Int)]
toMultiset [] = []
toMultiset (x:xs) = (x,(countNumbers x (x:xs))):(toMultiset (filter (not. (==x)) xs))

mUnion :: (Eq a) => [(a,Int)] -> [(a,Int)] -> [(a,Int)]
mUnion [] ys = ys
mUnion xs [] = xs
mUnion (x:xs) (y:ys) 
	| fst x ==  fst y = (greaterNumber x y):(mUnion xs ys)

greaterNumber ::(Eq a) => (a,Int) -> (a,Int) -> (a,Int)
greaterNumber x y 
	|fst x == fst y && snd x >= snd y = x
	|fst x == fst y && snd x <= snd y = y
	|otherwise = error "Not possible"

removeSet _  []    = []
removeSet [] ys    = ys
removeSet xs (y:ys)= if element y xs then removeSet xs ys else y:removeSet xs ys
  where element x [] = False
        element x (l:ls) = if l == x then True else element x ls

subset :: (Eq a) => [a] -> [a] -> Bool
subset [] ys = True 
subset (x:xs) ys
	| elem x ys = subset xs ys
	| otherwise = False	

countNumbers :: (Eq a) => a -> [a] -> Int 
countNumbers n xs = length(filter (==n) xs)
-- TEST SET FOR Q1
{-
Your functions should have the following behaviour:
complement [1,2,3] [1..5] = Just [4,5]
complement [1,2,3] [2..5] = Nothing
toMultiset [1,1,1,2,4,1,2] = [(1,4),(2,2),(4,1)]
toMultiset "from each according to his ability, to each according to his needs" = [('f',1),('m',1),('b',1),('l',1),('y',1),(',',1),('a',5),('c',6),('r',3),('g',2),('t',4),('o',6),('h',4),('i',6),(' ',11),('n',3),('e',4),('d',3),('s',3)]
mUnion [(1,6),(2,3)] [(1,2),(2,5),(3,1)] = [(1,6),(2,5),(3,1)]
mUnion [(1,2),(4,1)] [(1,1),(4,2)] = [(1,2),(4,2)]
 
THE ORDER OF ELEMENTS IN THE RESULTS OF mUnion IS NOT IMPORTANT.
-}

-- QUESTION 2: Functions and relations

symClosure ::(Eq a) => [(a,a)] -> [(a,a)]
symClosure xs = uniqueList(xs ++ map flipPair xs)

uniqueList :: (Eq a) => [a] -> [a]
uniqueList [] = []
uniqueList (x:xs)
        | elem x xs = uniqueList xs
        | otherwise = x : uniqueList xs

flipPair :: (a,a) -> (a,a)
flipPair (x,y) = (y,x)

invert :: [[a]] -> [[a]]
invert xs = map invertOne xs

invertOne :: [a] -> [a]
invertOne [x, y] = [y, x]
invertOne xs = xs

-- TEST SET FOR Q2
{-
Your functions should have the following behaviour:
symClosure [(1,2),(3,2)] = [(1,2),(3,2),(2,1),(2,3)]
symClosure [(1,1),(3,5)] = [(1,1),(3,5),(5,3)]

DO NOT WORRY ABOUT THE ORDER IN WHICH PAIRS APPEAR IN YOUR LIST
-}

-- QUESTION 3: Combinatorics

choose2 :: [Int] -> [(Int, Int)]
choose2 [] = []
choose2 (x:xs) = map ((,) x) xs ++ choose2 xs

-- TEST SET FOR Q3
{-
Your functions should have the following behaviour:
choose2 [1,2,3] = [(1,2),(1,3),(2,3)]
choose2 [2,6,9,12] = [(2,6),(2,9),(2,12),(6,9),(6,12),(9,12)]
NOTE THAT THE SMALLER ELEMENT IN EACH PAIR APPEARS FIRST. THE ORDERING OF THE PAIRS IN THE LIST DOES NOT MATTER.
-}

-- QUESTION 4: Primes

factors :: Int -> [Int]
factors n = [x | x <- [2..n], n `mod` x == 0]

{- Works the same as primeFactorisation 
prime_factors m = f m (head primes) (tail primes) where
  f m n ns
    | m < 2 = []
    | m < n ^ 2 = [m]   -- stop early
    | m `mod` n == 0 = n : f (m `div` n) n ns
    | otherwise = f m (head ns) (tail ns)

prime_factors :: Int -> [Int]
prime_factors n =
  case factors of
    [] -> [n]
    _  -> factors ++ prime_factors (n `div` (head factors))
  where factors = take 1 $ filter (\x -> (n `mod` x) == 0) [2 .. n-1]
-}

primeFactorisation :: Int -> [Int]
primeFactorisation n = iter n primes where
    iter n (p:_) | n < p^2 = [n | n > 1]
    iter n ps@(p:ps') =
        let (d, r) = n `divMod` p
        in if r == 0 then p : iter d ps else iter n ps'

isPrime :: Int -> Bool
primes :: [Int]

isPrime n | n < 2 = False
isPrime n = all (\p -> n `mod` p /= 0) . takeWhile ((<= n) . (^ 2)) $ primes
primes = 2 : filter isPrime [3..]
	
-- TEST SET FOR Q4
{-
Your functions should have the following behaviour:
factors 75 = [3,5,15,25,75]
factors 64 = [2,4,8,16,32,64]
primeFactorisation 75 = [3,5,5]
primeFactorisation 64 = [2,2,2,2,2,2]
-}

-- QUESTION 5: RSA

eTotient :: Int -> Int
eTotient n = length [x | x <- [1..n], coprime x n]

coprime a b = gcd a b == 1

encode :: Int -> Int -> Int -> Int -> Maybe Int
encode p q m e 
	| (isPrime p) && (isPrime q) && (coprime e (eTotient (p*q))) && e > 1 && (e < (eTotient (p*q))) = Just (mod (m^e) (p*q))
	| otherwise = Nothing

-- TEST SET FOR Q5
{-
Your functions should have the following behaviour:
eTotient 54 = 18
eTotient 73 = 72
encode 37 23 29 5 = Just 347
encode 99 18 108 45 = Nothing
encode 37 17 23 48 = Nothing
-}